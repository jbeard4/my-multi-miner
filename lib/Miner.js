/**
 * Copyright 2017 Google Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
const {helper} = require('./helper');
const Launcher = require('./Launcher');
const MinerFetcher = require('./MinerFetcher');

module.exports = class {
  /**
   * @param {string} projectRoot
   * @param {string} preferredRevision
   */
  constructor(projectRoot, preferredRevision) {
    this._projectRoot = projectRoot;
    this._launcher = new Launcher(projectRoot, preferredRevision);
  }

  /**
   * @param {!(Launcher.LaunchOptions & Launcher.ChromeArgOptions & Launcher.MinerOptions)=} options
   * @return {!Promise<!Miner>}
   */
  launch(options) {
    return this._launcher.launch(options);
  }

  /**
   * @return {string}
   */
  executablePath() {
    return this._launcher.executablePath();
  }

  /**
   * @param {!Launcher.ChromeArgOptions=} options
   * @return {!Array<string>}
   */
  defaultArgs(options) {
    return this._launcher.defaultArgs(options);
  }

  /**
   * @param {!MinerFetcher.Options=} options
   * @return {!MinerFetcher}
   */
  createMinerFetcher(options) {
    return new MinerFetcher(this._projectRoot, options);
  }
};

helper.tracePublicAPI(module.exports, 'Miner');
