const miner = require('../..')

//TODO: probably move this initialization stuff into the launcher
const childProcess = require('child_process');

const {stdout, status, stderr} = childProcess.spawnSync('npm',['whoami']);
const npmUsername = status === 0 ? stdout.toString().slice(0, -1) : undefined;
const pkgName = npmPkgNameToProhashingGroupName(require('./package.json').name)


/**
 * Rules for mapping npm package names to prohashing.com group names:
 *
 * npm:

 * The name must be less than or equal to 214 characters. This includes the
 * scope for scoped packages.  The name can’t start with a dot or an
 * underscore.  New packages must not have uppercase letters in the name.  The
 * name ends up being part of a URL, an argument on the command line, and a
 * folder name. Therefore, the name can’t contain any non-URL-safe characters.


 * prohashing:

 * The name of this miner's group, which may consist of letters, numbers, and
 * underscores. Grouing miners allows combined statistics to be viewed and
 * compared with other workers and groups. It is up to the customer to decide
 * how miners are to be grouped. Groups may be defined by algorithm, by
 * location, by mining rig type, or by any other factor.


 * How do we do this?

 * npm package cannot have uppercaes letters in the name. So we can use this as escape character. And convert to unicode char codes. e.g. "U0026__" instead of "&"
 */

function npmPkgNameToProhashingGroupName(name){
  const re = /[^a-zA-Z0-9_]/g;    //letters, numbers and underscores
  return name.replace(re, function(match, offset, string){
    return 'U' + match.charCodeAt(0) + '__';
  })
}


function prohashingGroupNameToNpmPkgName(name){
  const re = /U(\d+)__/g;    //escape code
  return name.replace(re, function(match, p1, offset, string){
    return String.fromCharCode(parseInt(p1));
  })
}


miner.launch({pkgName, npmUsername});
